class CreateRolesUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :roles_users do |t|
      t.references :user, index: true, foreign_key: true
      t.references :role, index: true, foreign_key: true
      t.datetime :deleted_at, default:nil
      t.timestamps
    end
  end
end
