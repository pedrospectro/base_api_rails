class CreateAddresses < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
      t.string :country, :null => false
      t.string :cep, :null => false
      t.string :uf, :null => false
      t.string :city, :null => false
      t.string :neigborhood
      t.string :street
      t.datetime :deleted_at, default:nil
      t.timestamps
    end
  end
end
