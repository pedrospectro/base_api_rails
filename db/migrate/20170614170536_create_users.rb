class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email, :null => false
      t.string :password
      t.date   :date_of_birth
      t.string :cpf
      t.string :street_number
      t.string :telephone
      t.string :api_key
      t.datetime :deleted_at, default:nil
      t.timestamps
    end
  end
end
