class CreateUserAddressReference < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :address, foreign_key: false
  end
end
