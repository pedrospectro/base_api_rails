class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.string :name, :null => false
      t.string :description, :null => false
      t.datetime :deleted_at, default:nil
      t.timestamps
    end
  end
end
