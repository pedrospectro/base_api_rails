#CREATING BASIC ROLES
@admin_role=Role.create(name:"admin",description:"Has full authorization")
@user_role=Role.create(name:"user",description:"Has some authorization")
#CREATING BASIC USERS
@admin_user=User.create(email:"test_admin@etools.com.br",password:Digest::SHA1.hexdigest("x6d4h3c9"))
@user=User.create(email:"test_user@etools.com.br",password:Digest::SHA1.hexdigest("teste123"))
@admin_user.update(api_key:'test_admin_token')
@user.update(api_key:'test_user_token')
#CREATING BASIC RELATIONSHIPS
@admin_relationship=RolesUser.create(user:@admin_user,role:Role.first)
@admin_relationship=RolesUser.create(user:@admin_user,role:Role.second)
@user_relationship=RolesUser.create(user:@user,role:Role.second)
#CREATING ADDRESS AND Relatioship between user/address
@address=Address.create(country:"Brasil",city:"curitiba",cep:"80030010",uf:"PR",street:"Luiz Leao",neigborhood:"CENTRO")
@admin_user.update(address_id:@address.id)