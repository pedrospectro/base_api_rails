# README

1) First config yor config/database.yml with ur mysql database user and password

2) Run the command 'bundle install' to install all dependencies

3) Run the command 'rails db:create' to create the database

4) Run the command 'rails db:migrate' to migrate the database

5) Run the seed 'rails db:seed'

6) Run the server 'rails s -p (port number) -b 0.0.0.0'
