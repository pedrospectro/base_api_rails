module Api::V1
    class AddressesController < ApiController
        before_action :set_address,only:[:show]

        def index
            render status: :not_found
        end

        def show
            render status: :ok,json:@address
        end

        def update
            render status: :not_found
        end

        def create
            @address=Address.create(@json)
            if @address
                render status: :created,json:@address
            else
                render status: :unprocessable_entity,json:{msg:"Erro ao criar"}
            end
        end

        def destroy
            render status: :not_found
        end

        private 
        def set_address
            @address=Address.find(params[:id])
        end
    end
end