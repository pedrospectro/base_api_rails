module Api::V1
    class UsersController < ApiController
        before_action :verify_user, only: [:show,:update,:destroy]

        def index
            render status: :not_found
        end

        def show
            render status: :ok,json:@current_user
        end

        def update
            @json['deleted_at']=@current_user.deleted_at
            if @current_user.update(@json)
                render status: :already_reported,json:@current_user
            else
                render status: :unprocessable_entity,json:{msg:"Erro ao atualizar usario"}
            end
        end
        
        def create
            render status: :not_found
        end
        
        def destroy
            if @current_user.update(deleted_at:Time.now())
                render status: :already_reported,json:@current_user
            else
                render status: :unprocessable_entity,json:{msg:"Erro ao remover usario"}
            end
        end

        private def verify_user
            if params[:id].to_i!=@current_user.id
                render status: :bad_request,json:{msg:"Voce nao pode alterar um usuário diferente"}
            end
        end

    end
end