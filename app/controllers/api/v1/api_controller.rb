module Api::V1
    class ApiController < ApplicationController
        include ActionController::HttpAuthentication::Token::ControllerMethods
        before_action :authenticate

        protected
        def authenticate
            authenticate_token || render_unauthorized
        end

        def authenticate_token
            authenticate_with_http_token do |token, options|
                @current_user = User.find_by(api_key: token,deleted_at:nil)
            end
        end

        def render_unauthorized(realm = "Application")
            self.headers["WWW-Authenticate"] = %(Token realm="#{realm.gsub(/"/, "")}")
            render status: :bad_request , json: {msg:"Unauthorized"}
        end
    end
end