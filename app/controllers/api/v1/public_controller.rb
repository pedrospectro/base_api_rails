module Api::V1
    class PublicController < ApplicationController
        before_action :stored_user, only: [:signup]

        def login
            #validate email uniqueness
            @user = User.where(email:@json["email"],password:Digest::SHA1.hexdigest(@json['password']))
                #.where("deleted_at is not NULL")
            if @user!=[]
                render status: :ok,json:@user.first
            else
                render status: :not_found
            end
        end

        def signup
            @json['password']=Digest::SHA1.hexdigest(@json['password'])
            @user = User.create(@json)
            if @user
                RolesUser.create(user:@user,role:Role.second)
                render status: :created,json:@user
            else
                render status: :unprocessable_entity
            end
        end

        def test
            render status: :ok
        end

        private def stored_user
            if User.where(email:@json['email'])!=[]
                render status: :unprocessable_entity,json: {msg:"Usuario com este email já está Cadastrado"}
            end
        end

    end
end