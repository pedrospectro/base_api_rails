module Api::V1::Admin
    class RolesUsersController < AdminController
        before_action :set_user_relationship
        before_action :set_role_relationship , only:[:show,:destroy]

        def show
            render json: @role
        end

        def index
            render json: @user.roles().where("roles.deleted_at is not NULL")
        end
        
        def update
            render status: :not_found
        end
        
        def create
            @roles_users = RolesUser.create(user:@user,role:Role.find(@json["role_id"]))
            if @roles_users
                render status: :created,json:@roles_users
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel criar a funcionalidade do usuario"}
            end
        end
        
        def destroy
            @role_users = RolesUser.find_by(role:@role,user:@user)
            if @role_users.update(deleted_at:Time.now())
                render status: :already_reported,json:@role_users
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel remover o papel do usuario"}
            end
        end

        private 
        def set_user_relationship
            @user=User.find(params[:user_id].to_i)
        end
        def set_role_relationship
            @role=@user.roles().find(params[:id].to_i)
        end
    end
end