module Api::V1::Admin
    class UsersController < AdminController
        before_action :stored_user, only:[:create]
        before_action :set_user, only:[:show,:update,:destroy]

        def show
            render json: @user
        end

        def index
            render json: User.where("deleted_at is not NULL")
        end
        
        def update
            if @user.update(@json)
                render status: :already_reported,json:@user
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel atualizar o user"}
            end
        end
        
        def create
            @user = User.create(@json)
            if @user
                RolesUser.create(user:@user,role:Role.second)
                render status: :created,json:@user
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel criar o user"}
            end
        end
        
        def destroy
            if @user.update(deleted_at:Time.now())
                render status: :already_reported,json:@user
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel remover o user"}
            end
        end

        private def stored_user
            if User.where(email:@json['email'])!=[]
                render status: :unprocessable_entity,json: {msg:"Usuario com este email já está Cadastrado"}
            end
        end
    end
end