module Api::V1::Admin
    class RolesController < AdminController
        before_action :set_role, only:[:show,:update,:destroy]

        def show
            render json: @role
        end

        def index
            render json: Role.where("deleted_at is not NULL")
        end
        
        def update
            if @role.update(@json)
                render status: :already_reported,json:@role
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel atualizar o papel"}
            end
        end
        
        def create
            @role = Role.create(@json)
            if @role
                render status: :created,json:@role
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel criar o papel"}
            end
        end
        
        def destroy
            if @role.update(deleted_at:Time.now())
                render status: :already_reported,json:@role
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel remover o papel"}
            end
        end
    end
end