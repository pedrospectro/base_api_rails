module Api::V1::Admin
    class AddressesController < AdminController
        before_action :set_address, only:[:show,:update,:destroy]

        def show
            render json: @address
        end

        def index
            render json: Address.where("deleted_at is not NULL")
        end
        
        def update
            if @address.update(@json)
                render status: :already_reported,json:@address
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel atualizar o endereco"}
            end
        end
        
        def create
            @address=Address.create(@json)
            if @address
                render status: :created,json:@address
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel criar o endereco"}
            end
        end
        
        def destroy
            if @address.update(deleted_at:Time.now())
                render status: :already_reported,json:@address
            else
                render status: :unprocessable_entity,json:{msg:"Impossivel remover o endereco"}
            end
        end
    end
end