class ApplicationController < ActionController::API
    before_action :get_json, 
    only:[
        :signup,
        :login,
        :create,
        :update
    ]

    private def get_json
        @json = JSON.parse(request.body.read)
    end
end