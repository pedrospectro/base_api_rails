Rails.application.routes.draw do
 
  #/API/V1/ ROUTES
  namespace :api  do
    namespace :v1 do

      #PUBLIC LOGIN SIGNUP
      get  'test'   , to:'public#test'
      post  'login' , to:'public#login'
      post  'signup', to:'public#signup'
      
      #PUBLIC RESOURCES
      resources :users,:addresses

      #ADMIN RESOURCES
      namespace :admin do
        
        #Basic Resources
        resources :roles,:addresses,:users

        #/users/:id/roles_users/:id management
        resources :users do
          resources :roles_users
        end
      
    end
      
    end
  end

end